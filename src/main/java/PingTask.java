import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

public class PingTask extends TimerTask {
    private Timer timer;
    private Socket s;
    private int retry = 0;

    PingTask(Timer timer, Socket s) {
        this.timer = timer;
        this.s = s;
    }

    public void run() {
        try {
            Socket s = new Socket(Client.serverHost, Client.serverPort); // dedicate another socket for solely pinging
            ObjectOutputStream dOut = new ObjectOutputStream(s.getOutputStream());
            if (Message.sendPing(dOut)) retry = 0;
            else retry += 1;

            if (retry == 3) {
                System.out.println("Ping timeout");
                // Terminate as server already consider as dead
                timer.cancel();
                s.close();
            }
        } catch (IOException e) {
            System.out.println("Failed to make socket to send ping");
            e.printStackTrace();
            timer.cancel();
            // potentially buggy code
            try {
                s.close(); // if cannot ping, always terminate the socket to unblock main thread
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }
}
