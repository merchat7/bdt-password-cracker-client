import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.SocketTimeoutException;

public class Message {
    public static boolean sendRequest(ObjectOutputStream dOut, String request) {
        try {
            dOut.writeByte(1);
            dOut.writeUTF(request);
            dOut.flush();
            System.out.println("Request send to server");
            return true;
        } catch (IOException e) {
            System.out.println("Failed to send request to server");
            e.printStackTrace();
            return false;
        }
    }

    public static boolean receiveAckRequest(ObjectInputStream dIn, String request) throws SocketTimeoutException {
        try {
            String messageType = dIn.readUTF();
            String message = dIn.readUTF();
            if (messageType.equals("received") && message.equals(request)) return true;
        } catch (SocketTimeoutException e) {
            // Timeout = 15 seconds, so should terminate!
            throw new SocketTimeoutException();
        } catch (IOException e) {
            System.out.println("Failed to receive ack for request from server");
            e.printStackTrace();
        }
        return false;
    }

    public static boolean sendPing(ObjectOutputStream dOut) {
        try {
            dOut.writeByte(3);
            dOut.writeUTF("ping");
            dOut.flush();
            //System.out.println("Ping send to server");
            return true;
        } catch (IOException e) {
            System.out.println("Failed to ping server");
            e.printStackTrace();
            return false;
        }
    }
}
