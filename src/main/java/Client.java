import org.apache.commons.codec.digest.Crypt;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Timer;

public class Client {
    static String serverHost;
    static int serverPort;

    public static void main(String[] args) {
        serverHost = args[0];
        serverPort = (Integer.parseInt(args[1]));
        String pwd = args[2];
        String request = Crypt.crypt(pwd, "ic");
        try {
            Socket s = new Socket(serverHost, serverPort); // the server host & listening port
            s.setSoTimeout(15000); // 15 seconds as server will assume client is dead after this time
            ObjectOutputStream dOut = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream dIn = new ObjectInputStream(s.getInputStream());

            handleRequest(dIn, dOut, request);
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new PingTask(timer, s), 0, 5000);

            s.setSoTimeout(0); // Block & wait for result forever
            System.out.println("Waiting for result...");
            String result = dIn.readUTF();
            System.out.println(String.format("Result is \"%s\"", result));
            timer.cancel(); // No effect if already cancelled
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void handleRequest(ObjectInputStream dIn, ObjectOutputStream dOut, String request) throws InterruptedException {
        try {
            int retry = 0;
            while (retry < 3) {
                if (!Message.sendRequest(dOut, request)) {
                    retry += 1;
                    Thread.sleep(1000);
                    continue;
                } else retry = 0;

                if (Message.receiveAckRequest(dIn, request)) break;
                else {
                    // Request packet was somehow corrupted or other errors
                    retry += 1;
                    Thread.sleep(1000);
                    // continue;
                }
            }
            if (retry == 3) throw new SocketTimeoutException();
        } catch (SocketTimeoutException e) {
            System.out.println("Time out limited exceeded");
            System.exit(1);
        }
    }
}
